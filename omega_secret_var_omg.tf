variable "yd_token_n_ids" {
  type = object({
    AMI_token    = string
    cloud_id = string
    folder_id = string
  })
  sensitive = true
}

