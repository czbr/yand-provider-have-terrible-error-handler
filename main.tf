terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.61.0"
    }
  }
}

provider "yandex" {
  token                    = var.yd_token_n_ids.AMI_token
  cloud_id                 = var.yd_token_n_ids.cloud_id
  folder_id                = var.yd_token_n_ids.folder_id
  zone                     = "ru-central1-b"
}


resource "yandex_compute_instance" "ya_1" {
    #name = ya_1

    resources {
        cores = 2
        memory = 2
    }

    boot_disk {
        initialize_params {
            image_id = "fd83klic6c8gfgi40urb"
        }
    }

    network_interface {
        subnet_id = data.yandex_vpc_subnet.foo.id
        nat_ip_address = resource.yandex_vpc_address.foo.external_ipv4_address[0].address
    }

    metadata = {
        foo = "bar"
        ssh-keys = "levinol:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDG8ObKVe2W/dRmFy51VfdRVetYVW5lb028/WPNRm7eJp3ykzpyy7+3KzSDrxXuMONUOO9/20P89OJkEhr15SafqQXY8UvAP3BR+He4U5lk/mJD8TG5yOIGwTmfs0IhASCyCCjwTmiiiTAttAi2RdtTEH3tOYTPLTM8KBry7zGj0pMWpkFEE7BxtB7Ma5c/G/IzXL0QMeiM3i81hGVAjvkaZjGewehvWZiEeepvy+/Pu4RiH0OFlnFIwz1DZtB+vJai+FkT13aNYk1YvsZ/RdthcpoKgVsVQlZYKcqSd37cPR+p/io7IthlgqAndUd3jmP/nYlM+OzJFhZFBsiWv39qEXcGvh/jjXuPInJJHa84qQ99eOb2rDlHKB9gK5ezMD4N99VB+oql08V+3VfDvwsC5Sf8iPwpuew9oA2FdJap//f09Kg4oiY4k52PRifS1iqDcN0Ti7YfqjKQjPTsYu5H35/FMRNRlgcLOjRiUBdWwnyaVYNXkmRFu82hDqaHO+cR8Ah9VshYupY8NdiPPlyRu/kquyBXhaEFPlZLSRgpQtNTdysLzQrmNlXZuk6tjFVUVBVsroPjql21bgADCu7atjcTcHqRVUaBw9LOQLwo9+Hx7xLjXGmN+yEjOc/d/xsOsOdu5/UPVFa8buttGnAh78V3zSZs1r1RPQX/wneC8w=="
    }
}

resource "yandex_vpc_address" "foo" {
  name = "Addressio examplio"

  external_ipv4_address {
    zone_id = "ru-central1-b"
  }
}

data "yandex_vpc_network" "foo"{
    network_id = "enpujc84m1vtq0ge1ot8"
}

data "yandex_vpc_subnet" "foo" {
  subnet_id = "e2ld4n40stc5atnpqcju"
}
